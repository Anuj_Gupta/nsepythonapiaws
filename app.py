from chalice import Chalice
import json
from nsetools import Nse


app = Chalice(app_name='stockprice')


@app.route('/')
def index():
    return {'hello': 'world'}


@app.route('/getstockprice', methods=['POST'])
def get_Stock_Price():
    nseObj = Nse()
    # print("Getting request")
    content = (app.current_request.json_body)
    ConfidentLevel = content["queryResult"]["intentDetectionConfidence"]
    if ConfidentLevel > 0.6:
        IntentType = content["queryResult"]["intent"]["displayName"]
        if IntentType == "ask_stock_price":
            stockName = content["queryResult"]["parameters"]["Stock_Name"]
            response = GetStockPrice(stockName, nseObj)
        elif IntentType == "Gainer_N_Losers":
            isGainers = content["queryResult"]["parameters"]["Gainer"] == "gainer"
            isLosers = content["queryResult"]["parameters"]["Losers"] == "loser"
            # print (isGainers)
            # print (isLosers)
            response = GetGainerLosers(isGainers, isLosers, nseObj)
    else:
        response = {"fulfillmentText": json.dumps(
            "Sorry we are unable to understand what your were trying to ask")}
    return response


def GetStockPrice(StockName, nseObj):
    if StockName:
        try:
            isValid = nseObj.is_valid_code(StockName)
            if isValid:
                response = nseObj.get_quote(StockName)
                # answer = "Company Name {} \n".format(response["companyName"])
                # answer += "Open price is Rs. {}\n".format(
                #     str(response["lastPrice"]))
                # answer += "Base price is Rs. {}\n".format(
                #     str(response["basePrice"]))
                # answer += "Average price  is Rs. {}\n".format(
                #     str(response["averagePrice"]))
                # answer += "Total Traded Volume  is Rs. {}\n".format(
                #     str(response["totalTradedVolume"]))
                # answer += "Variable Margin  is Rs. {}\n".format(
                #     str(response["varMargin"]))
                # answer += "\n\n Would you like to buy this stock?."
                # return (jsonify({"fulfillmentText" : answer}),200)
                # print(answer)
                response = {"fulfillmentText": "Success",
                            "fulfillmentMessages": [{"text": {"text": [json.dumps(response)]}}]}
            else:
                # return make_response(jsonify({"Status" : 1, "Message": "Invalid Stock Type"}),200)
                response = {"fulfillmentText": "Error",
                            "fulfillmentMessages": [{"text": {"text":  ["Invalid Stock Type"]}}]}
        except:
            # return make_response(jsonify({"Status" : -1, "Message": "Exception"}),500)
            response = {"fulfillmentText": "Exception",
                        "fulfillmentMessages": [{"text": {"text": ["Exception"]}}]}
    else:
        response = {"fulfillmentText": "Error",
                    "fulfillmentMessages": [{"text": {"text": ["Empty StockName"]}}]}
    return response


def GetGainerLosers(isGainer, isLoser, nseObj):
    if isGainer and isLoser:
        Gainerresponse = nseObj.get_top_gainers()
        Loserresponse = nseObj.get_top_losers()
        # GainerHtml = "\n ------Top Gainer's------ \n"
        # for GainerVal in Gainerresponse:
        #     GainerHtml += "\n ------{}------ \n".format(GainerVal["symbol"])
        #     # GainerHtml += "Company Symbol {} \n".format(GainerVal["symbol"])
        #     GainerHtml += "openPrice Rs {} \n".format(GainerVal["openPrice"])
        #     GainerHtml += "highPrice Rs {} \n".format(GainerVal["highPrice"])
        #     GainerHtml += "lowPrice Rs {} \n".format(GainerVal["lowPrice"])
        #     GainerHtml += "Last trading price Rs {} \n".format(
        #         GainerVal["ltp"])
        #     GainerHtml += "previousPrice Rs {} \n".format(
        #         GainerVal["previousPrice"])
        #     GainerHtml += "tradedQuantity {} \n".format(
        #         GainerVal["tradedQuantity"])
        #     GainerHtml += "-------------- \n\n"

        # LoserHtml = "\n ------Top Losers------ \n"
        # for LoserVal in Loserresponse:
        #     LoserHtml += "\n ------{}------ \n".format(LoserVal["symbol"])
        #     LoserHtml += "openPrice Rs {} \n".format(LoserVal["openPrice"])
        #     LoserHtml += "highPrice Rs {} \n".format(LoserVal["highPrice"])
        #     LoserHtml += "lowPrice Rs {} \n".format(LoserVal["lowPrice"])
        #     LoserHtml += "Last trading price Rs {} \n".format(LoserVal["ltp"])
        #     LoserHtml += "previousPrice Rs {} \n".format(
        #         LoserVal["previousPrice"])
        #     LoserHtml += "tradedQuantity {} \n".format(
        #         LoserVal["tradedQuantity"])
        #     LoserHtml += "-------------- \n\n"
        GainerLossersData = {"Gainer": Gainerresponse, "Loser": Loserresponse}
        response = {"fulfillmentText": "Success", "fulfillmentMessages":
                    [{"text": {"text": [json.dumps(GainerLossersData)]}}]}

    elif isGainer:
        Gainerresponse = nseObj.get_top_gainers()
        # GainerHtml = "\n ------Top Gainer's------ \n"
        # for GainerVal in Gainerresponse:
        #     GainerHtml += "\n ------{}------ \n".format(GainerVal["symbol"])
        #     # GainerHtml += "Company Symbol {} \n".format(GainerVal["symbol"])
        #     GainerHtml += "openPrice Rs {} \n".format(GainerVal["openPrice"])
        #     GainerHtml += "highPrice Rs {} \n".format(GainerVal["highPrice"])
        #     GainerHtml += "lowPrice Rs {} \n".format(GainerVal["lowPrice"])
        #     GainerHtml += "Last trading price Rs {} \n".format(
        #         GainerVal["ltp"])
        #     GainerHtml += "previousPrice Rs {} \n".format(
        #         GainerVal["previousPrice"])
        #     GainerHtml += "tradedQuantity {} \n".format(
        #         GainerVal["tradedQuantity"])
        #     GainerHtml += "-------------- \n\n"
        response = {"fulfillmentText": "Success",
                    "fulfillmentMessages": [{"text": {"text":  [json.dumps(Gainerresponse)]}}]}
    else:
        Loserresponse = nseObj.get_top_losers()
        # LoserHtml = "\n ------Top Losers------ \n"
        # for LoserVal in Loserresponse:
        #     LoserHtml += "\n ------{}------ \n".format(LoserVal["symbol"])
        #     LoserHtml += "openPrice Rs {} \n".format(LoserVal["openPrice"])
        #     LoserHtml += "highPrice Rs {} \n".format(LoserVal["highPrice"])
        #     LoserHtml += "lowPrice Rs {} \n".format(LoserVal["lowPrice"])
        #     LoserHtml += "Last trading price Rs {} \n".format(LoserVal["ltp"])
        #     LoserHtml += "previousPrice Rs {} \n".format(
        #         LoserVal["previousPrice"])
        #     LoserHtml += "tradedQuantity {} \n".format(
        #         LoserVal["tradedQuantity"])
        #     LoserHtml += "-------------- \n\n"

        response = {"fulfillmentText": "Success",
                    "fulfillmentMessages": [{"text": {"text": [json.dumps(Loserresponse)]}}]}

    return response


# The view function above will return {"hello": "world"}
# whenever you make an HTTP GET request to '/'.
#
# Here are a few more examples:
#
# @app.route('/hello/{name}')
# def hello_name(name):
#    # '/hello/james' -> {"hello": "james"}
#    return {'hello': name}
#
# @app.route('/users', methods=['POST'])
# def create_user():
#     # This is the JSON body the user sent in their POST request.
#     user_as_json = app.current_request.json_body
#     # We'll echo the json body back to the user in a 'user' key.
#     return {'user': user_as_json}
#
# See the README documentation for more examples.
#
